Provision Workstation
=====================

The `provision_workstation` Ansible role helps user's finish provisioning their Linux workstation with the necessary components to hit the ground running. This has been tested to work on Fedora & CentOS Linux distributions.

Requirements
------------

- Ansible must be installed.
- The user running this Ansible role must have administrative permissions on the system


Role Variables
--------------

|  Variable Name 	| Description  	| How To Generate Value  	|
|---	|---	|---	|
|  `firstname` 	| The first name of the new user to create.  	| By default, Ansible will prompt to provide a value for this variable. You can also hardcode the value (defaults.yml) or pass in an extra argument using `-e firstname=jon` flag. 	|
|  `lastname` 	| The last name of the new user to create. 	|  By default, Ansible will prompt to provide a value for this variable. You can also hardcode the value (defaults.yml) or pass in an extra argument using `-e lastname=murillo` 	|
|  `password` 	| The password of the new user to create. 	|  By default, Ansible will prompt to provide a value for this variable. You can also hardcode the value (defaults.yml) or pass in an extra argument using `-e password=Welcome2019!` 	|
|  `username` 	| The username that will be used in ALL configs. 	|  By default, Ansible take the first letter of the entered firstname , combine it with the lastname and append `_sa` to the end.	|

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```
- hosts: localhost
  become: true
  vars_prompt:
    - name: 'firstname'
      prompt: 'Enter your firstname'
      private: no

    - name: 'lastname'
      prompt: 'Enter your lastname'
      private: no

    - name: 'password'
      prompt: 'Enter password for the new user we will create'
      private: yes
      encrypt: "sha512_crypt"
      confirm: yes
      salt_size: 7
  roles:
    - provision_workstation
```

License
-------

- IDEMIA | Engineering | Identity & Security, N.A.


Author Information
------------------

- Jon Murillo | [Email Jon  Murillo](mailto:jon.murillo@us.idemia.com?subject=[Provision_Workstation_Question]%20Source%20Han%20Sans)
- Stephen Hendrix | [Email Stephen Hendrix](mailto:stephen.hendrix@us.idemia.com?subject=[Provision_Workstation_Question]%20Source%20Han%20Sans)
- Systems Distro | [Email Entire Team](mailto:esd-eng-systems@us.idemia.com?subject=[Provision_Workstation_Question]%20Source%20Han%20Sans)
