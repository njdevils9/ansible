# ASSUMPTIONS
* DHCP on the machine being imaged.
* The user will run the script that kickstart will put in their home directory! Running rebuild_laptop.sh should install all of the software that you'll need.


# BUILD THE IMAGE

#### Create working directories
```bash
mkdir /mnt/temp /mnt/newiso
```

#### Mount downloaded iso for copying files from provided installer to your custom built one.
```bash
mount /path/to/iso/CentOS-7-aarch64-NetInstall-1908.iso /mnt/temp/
```

#### Copy files from downloaded ISO to your temporary directory.
```bash
rsync -azP /mnt/temp/ /mnt/newiso/
```

#### EDIT isolinux.cfg from the copied ISO or copy the version from this repository to your newiso directory to add menu entries for Kickstart build

### Copy your kickstart.cfg, rebuild_laptop.sh, rebuild_laptop.yml and files directory to the same directory of your ISO.
```bash
rsync -azP kickstart.cfg rebuild_laptop.sh rebuild_laptop.yml files /mnt/newiso
```

#### Now that your files are in place, build your ISO.
```bash
sudo /usr/bin/genisoimage -untranslated-filenames -volid 'CentOS-7-x86_64' -J -joliet-long -rational-rock -translation-table -input-charset utf-8 -x ./lost+found -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot -o /opt/centos7_custom.iso -T /mnt/newiso
```

#### ISO post-processing to build UEFI bootable image.
```bash  
isohybrid -u /home/$( whoami )/centos7_custom.iso
```

#### Copy image to your USB, assuming it's mounted at /dev/sdb
```bash
dd if=/home/$( whoami )/centos7_custom.iso of=/dev/sdb
```


# EXAMPLE: isolinux.cfg #
```bash
label linux_ks
  menu label ^Install CentOS-7 with Kickstart
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS-7-x86_64:/ ks=hd:LABEL=Fedora-WS-23-x86_64:/kickstart.cfg.cfg
```

# How do I set my user password?
#### Modify this line in the kickstart.cfg file
```bash
user --groups=wheel --name=jyoung --password=$6$asdfjklaasdfjkla$Xh995/tlASi/d3UwNfotHVnLJT1ATI95r5WBBME.yo50bN.6.hFcKCDP3BxXJoaSq2cG1G/kopyHQoz65MMwq1 --iscrypted --gecos="Jon Murillo"
```

#### Generate the SHA512 has using this command:
```bash
## The salt used can be whatever you'd like.
/usr/bin/python2 -c 'import crypt, getpass; print crypt.crypt(getpass.getpass(), "$6$asdfjkl;asdfjkl;")'
```
